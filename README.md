# Laravel Test Project (php Laravel)

## Introduction

An introduction or lead on what problem you're solving. Answer the question, "Why does someone need this?"

## Installation

### Create your .env File
> you can copy and paste .env.example file ;)

### Install composer packages
bash
$ composer install


### Generate Application Key
bash
$ php artisan key:generate

### Migrate Database
bash
$ php artisan migrate

### Seeding Database
bash
$ php artisan db:seed

> You can get postman collication from src folder (laravel test.postman_collection.json)
