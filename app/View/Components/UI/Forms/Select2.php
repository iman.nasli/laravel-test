<?php

namespace App\View\Components\UI\Forms;

use Illuminate\View\Component;

class Select2 extends Component
{
    public $name ;
    public $placeholder ;
    public $readonly ;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($name,$placeholder,$readonly = false)
    {
        $this->placeholder = $placeholder;
        $this->name = $name;
        $this->readonly = $readonly;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.UI.forms.select2');
    }
}
