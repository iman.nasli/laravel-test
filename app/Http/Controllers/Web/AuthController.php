<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Login;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Login $request)
    {
        $credentials = $request->only('username', 'password');
        $remember_me = isset($request->remember_me);
        if (Auth::attempt($credentials,$remember_me)) {
            return redirect()->route('index');
        }
        return redirect()->back();
    }
}
