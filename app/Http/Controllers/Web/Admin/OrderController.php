<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderProduct;
use App\Models\Product;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::get();
        return view('pages.orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customers = Customer::get();
        $products = Product::get();
        $edit = false;
        $readonly = false;
        return view('pages.orders.edit-add', compact('customers', 'products', 'readonly', 'edit'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string'],
        ]);
        $data = $request->except(['_token']);
        $order = Order::create($data);
        foreach ($data['products'] as $product) {
            $order->products()->attach($product);
        }
        return redirect()->route('orders.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::find($id);
        $customers = Customer::get();
        $products = $order->products()->get();
        $readonly = true;
        $edit = false;
        return view('pages.orders.edit-add',compact('order', 'customers', 'products','readonly','edit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $order = Order::findOrFail($id);
        $customers = Customer::get();
        $products = Product::get();
        $edit = true;
        $readonly = false;
        return view('pages.orders.edit-add',compact('order', 'customers', 'products','edit','readonly'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => ['required', 'string'],
        ]);
        $data = $request->except(['_method','_token']);
        $order = Order::findOrFail($id);
        $order->update($data);
        foreach ($data['products'] as $product) {
            if($order->products()->where('id', $product)->get()->isEmpty())
                $order->products()->attach($product);
        }
        return redirect()->route('orders.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::destroy($id);
        return redirect()->route('orders.index');
    }
}
