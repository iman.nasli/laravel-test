<?php

use Illuminate\Support\Facades\Auth;

if (!function_exists('user')) {
    function user($guard = '')
    {
        return Auth::user($guard);
    }
}

if (!function_exists('check_user')) {
    function check_user($guard = '')
    {
        return Auth::guard($guard)->check();
    }
}
