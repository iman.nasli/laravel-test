<?php

use App\Http\Controllers\Web\Admin\{
    CustomerController, OrderController, ProductController, UserController
};
use App\Http\Controllers\Web\AuthController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::redirect('/', '/admin', 201);

Route::group(['prefix' => 'admin'], function () {
    Route::group(['middleware' => ['guest']], function () {
        Route::post('login',[AuthController::class,'login'])->name('login');
        Route::view('/login','pages.login');
    });

    Route::group(['middleware' => ['auth']], function () {
        Route::view('/', 'index')->name('index');
        Route::view('dashboard', 'index')->name('dashboard');
        Route::resource('customers', CustomerController::class);
        Route::resource('products', ProductController::class);
        Route::resource('orders', OrderController::class);
        Route::resource('users', UserController::class);
    });

});
