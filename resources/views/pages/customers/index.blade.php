<x-layout.data-table title="{{__('Costumers')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class=" dt-responsive">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap mt-5">
                        <thead>
                        <tr>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Phone')}}</th>
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($customers as $customer)
                            <tr>
                                <td>{{$customer->name}}</td>
                                <td>{{$customer->phone}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</x-layout.data-table >
