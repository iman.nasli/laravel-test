<x-layout.data-table title="{{__('Products')}}">
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class=" dt-responsive">
                    <table id="datatable" class="table table-bordered dt-responsive nowrap mt-5">
                        <thead>
                        <tr>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Create At')}}</th>
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($products as $product)
                            <tr>
                                <td>{{$product->name}}</td>
                                <td>{{$product->created_at->format('d-M-Y')}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</x-layout.data-table >
