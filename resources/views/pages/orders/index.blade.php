<x-layout.data-table title="{{__('Orders')}}">

    @push('plugin-css')
    <link rel="stylesheet" href="{{asset('assets/libs/datatables/buttons.bootstrap4.css')}}">
    @endpush
    <div class="row">
        <div class="col-12">
            <div class="card-box">
                <div class="d-inline-block mt-0 header-title mb-2">
                    <div class="float-left">
                        <a href="{{route('orders.create')}}" class="btn btn-success waves-effect  waves-light">
                            <i class="fas fa-plus"></i>
                            {{__('Add Order')}}
                        </a>
                    </div>

                </div>
                <div class=" dt-responsive">
                    <table id="orders" class="table table-bordered dt-responsive nowrap mt-5">
                        <thead>
                        <tr>
                            <th>{{__('#')}}</th>
                            <th>{{__('Name')}}</th>
                            <th>{{__('Customer')}}</th>
                            <th>{{__('Created At')}}</th>
                            <th data-orderable="false">{{__('Actions')}}</th>
                        </tr>
                        </thead>


                        <tbody>
                            @foreach ($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->name}}</td>
                                <td>{{$order->customers()->first()->name}}</td>
                                <td>{{$order->created_at->format('d-M-Y')}}</td>
                                <td>
                                    <a href="{{route('orders.show',['order'=>$order->id])}}" class="btn btn-info waves-effect btn-sm waves-light">
                                        <i class="far fa-eye"></i>
                                        {{__('View')}}
                                    </a>
                                    <a href="{{route('orders.edit',['order'=>$order->id])}}" class="btn btn-warning waves-effect btn-sm waves-light">
                                        <i class=" fas fa-edit"></i>
                                        {{__('Edit')}}
                                    </a>
                                    <a  href="javascript" onclick="$('#modal-form').attr('action','{{route('orders.destroy',['order'=>$order->id])}}');
                                        $('#delete-name').text('{{$order->name}}')""  data-toggle="modal" data-target="#modal" class="btn btn-danger  waves-effect btn-sm waves-light">
                                        <i class="fas fa-trash"></i>
                                        {{__('Delete')}}
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <x-UI.modal.scroll-modal :title="__('Delete',['attr'=>__('Order')])" :body="__('Order')"/>
        @push('plugin-js')
        {{-- <script src="{{asset("assets/libs/datatables/dataTables.buttons.min.js")}}"></script> --}}
        {{-- <script src="{{asset("assets/libs/datatables/buttons.html5.min.js")}}"></script> --}}
        {{-- <script src="{{asset("/assets/libs/jszip/jszip.min.js")}}"></script> --}}
        @endpush

        @push('javascript')
<script>
    $(document).ready(function() {
            var table = $('#orders').DataTable( {
                orderCellsTop: true,
                fixedHeader: true,
                dom: 'Bfrtip',
                buttons: [
                    {
                        extend :'excel',
                        text:"Export to Excel File",
                        exportOptions: {
                            columns: [ 0, 1, 2, 3 ],
                        },
                        className: 'btn btn-success waves-effect  waves-light',
                    }
                ]
            });
        });
</script>
        @endpush
</x-layout.data-table >
