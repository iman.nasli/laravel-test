
<x-layout.form :title="__('Add Orders')">
    <div class="row">
        <div class="col-xl-12">
            <div class="card-box">
                {{-- <h4 class="header-title mt-0 mb-3">Horizontal Form</h4> --}}

                <form class="form-horizontal" user="form" method="POST" action="{{($edit?? false) ?route('orders.update',['order'=>$order->id]):  route('orders.store') }}" data-parsley-validate novalidate autocomplete="off">
                    @if ($edit)
                        @method('put')
                    @endif
                    @if ($readonly)
                        @method('get')
                    @endif
                    @csrf
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="name">{{__('Name')}}*</label>
                            <x-UI.forms.input :readonly=$readonly name="name" :value="old('name',$order['name']??'')" :placeholder="__('Name')" />
                        </div>
                        <div class="form-group col-md-6">
                            <label for="username">{{__('Customer')}}*</label>
                            <x-UI.forms.select2 :readonly=$readonly name="customer_id" :placeholder="__('Customers')">
                                @foreach ($customers as $customer)
                                    <option value="{{$customer->id}}" {{old('customer_id', $order->customer_id ?? '') == $customer->id ? 'selected' : ''}}>
                                        {{$customer->name}}
                                    </option>
                                @endforeach
                            </x-UI.forms.select2>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="form-group col-md-6">
                            <label for="username">{{__('Products')}}*</label>
                            @foreach ($products as $product)
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" {{$readonly || ($edit && !$order->products()->get()->where('id', $product->id)->isEmpty()) ? "checked" : ''}} {{$readonly ? 'disabled':''}} id="product_{{$product->id}}" name="products[]" value="{{$product->id}}">
                                    <label class="custom-control-label" for="product_{{$product->id}}">{{$product->name}}</label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="form-group row mt-5">
                        <div class="offset-sm-4 col-sm-8">
                            @if (!$readonly)
                            <button type="submit" class="btn btn-primary waves-effect waves-light mr-1">
                                {{ ($edit ? __('Edit'):__('Add',['attr'=>' '])) }}
                            </button>
                            @endif
                            <a  href="{{route('orders.index')}}"
                                    class="btn btn-secondary waves-effect waves-light">
                                {{__('Back')}}
                            </a>
                        </div>
                    </div>

                </form>
            </div>
        </div><!-- end col -->
    </div>
</x-layout.form>
